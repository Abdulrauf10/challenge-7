const router = require('express').Router();
const restrict = require('./middleware/restrict');
const auth = require('./controllers/authController');

const {addUser, saveUser, userDelete, userUpdateGet, userUpdatePost} = require ('./controllers/userController');
const {dashboard} = require ('./controllers/dashboardController');
const {getAllUserGame, updateUser} = require ('../controllers/userApiController');

router.get('/', restrict, (req, res) => {
    res.render('/dashboard', {user: req.user})
})

router.get('/register', (req, res) => res.render('register'));
router.post('/register', auth.register);

router.get('/login', (req, res) => res.render('login'));
router.post('/login', auth.login);

router.get('/whoami', restrict, auth.whoami);


// dashboard
router.get('/dashboard', dashboard);
  
// userControllers
router.get('/user/add', addUser)
router.post('/user/save', saveUser)
router.get('/user/delete/:id', userDelete)
router.get('/user/update/:id', userUpdateGet)
router.post('/user/update/:id', userUpdatePost )

router.get('/', getAllUserGame);
router.post('/update/:id', updateUser);

module.exports = router;

