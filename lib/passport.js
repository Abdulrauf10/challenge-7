const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const { User } = require('../models');
const { Strategy: JWTStrategy, ExtractJwt } = require('passport-jwt');

async function authenticate(username, password, done){
    try{
        const user = await User.authenticate({username, password})

        return done(null, user)
    } catch(err){
        return done(null, false, {message: err.message})
    }
}

passport.use(
    new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
    }, authenticate)
)

passport.serializeUser(
    (user, done) => done(null, user.id)
)

const options = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: 'keyinirahasia',
}

passport.deserializeUser(async (id, done) => {
        // done(null, User.findByPk(id))
        const user = await User.findByPk(id);
        return done(null, user);
    }
)

module.exports = passport;
