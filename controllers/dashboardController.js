const {user_game} = require ('../models');

module.exports = {
    dashboard: (req, res) => {
        if (req.session.loggedIn) {
            user_game.findAll().then(userGame => {
              res.render('dashboard', { userGame });
            })
          } else {
            res.redirect('/login')
          }
        }
}