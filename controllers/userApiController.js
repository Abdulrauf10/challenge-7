const {user_game, user_game_data, user_game_history} = require ('../models');

module.exports = {
    getAllUserGame: (req, res) => {
        user_game.findAll().then(userGame => {
            user_game_data.findAll().then(userId => {
                user_game_history.findAll().then(all => {
                    res.status(200).json({ userGame, userId, all })
                })
            })
        })
    },
    updateUser:  (req, res) => {
        const userId = req.params.id;
      
        user_game.update({
          name: req.body.name,
          email: req.body.email,
          credit: req.body.credit
        }, {
          where: {
            id: userId
          }
        }).then(user => {
          user_game_data.update({
            alamat: req.body.alamat,
            kelamin: req.body.kelamin
          }, {
            where: {
              user_game_id: userId
            }
          }).then(biodata => {
            user_game_history.update({
                win: req.body.win,
                lose: req.body.lose
            }, {
                where:{
                    id: userId
                }
            }).then(view => {
                res.status(200).json({ message: 'berhasil update data'})
            })
          })
        })
      }
}